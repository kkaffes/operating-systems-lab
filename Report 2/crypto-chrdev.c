/*
 * crypto-chrdev.c
 *
 * Implementation of character devices
 * for virtio-crypto device 
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr>
 *
 */
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/wait.h>
#include <linux/virtio.h>
#include <linux/virtio_config.h>

#include "crypto.h"
#include "crypto-chrdev.h"
#include "debug.h"

#include "/usr/include/crypto/cryptodev.h"


#define BLOCK_SZ      16
#define KEY_SIZE        24

/*
 * Global data
 */
struct cdev crypto_chrdev_cdev;

/**
 * Given the minor number of the inode return the crypto device 
 * that owns that number.
 **/
static struct crypto_device *get_crypto_dev_by_minor(unsigned int minor)
{
	struct crypto_device *crdev;
	unsigned long flags;

	debug("Entering");

	spin_lock_irqsave(&crdrvdata.lock, flags);
	list_for_each_entry(crdev, &crdrvdata.devs, list) {
		if (crdev->minor == minor)
			goto out;
	}
	crdev = NULL;

out:
	spin_unlock_irqrestore(&crdrvdata.lock, flags);

	debug("Leaving");
	return crdev;
}

/*************************************
 * Implementation of file operations
 * for the Crypto character device
 *************************************/

static int crypto_chrdev_open(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	unsigned int len;
	struct crypto_open_file *crof;
	struct crypto_device *crdev;
	unsigned int syscall_type = VIRTIO_CRYPTO_SYSCALL_OPEN;
	int host_fd = -1;

	struct virtqueue *vq;
	struct scatterlist syscall_type_sg, fd_sg, 
						*sgs[2];

	unsigned int num_out, num_in;

	debug("Entering");


	num_out = 0;
	num_in = 0;

	ret = -ENODEV;
	if ((ret = nonseekable_open(inode, filp)) < 0)
		goto fail;

	/* Associate this open file with the relevant crypto device. */
	crdev = get_crypto_dev_by_minor(iminor(inode));
	if (!crdev) {
		debug("Could not find crypto device with %u minor", 
		      iminor(inode));
		ret = -ENODEV;
		goto fail;
	}

	crof = kzalloc(sizeof(*crof), GFP_KERNEL);
	if (!crof) {
		ret = -ENOMEM;
		goto fail;
	}
	crof->crdev = crdev;
	crof->host_fd = -1;
	filp->private_data = crof;

	/**
	 * We need two sg lists, one for syscall_type and one to get the 
	 * file descriptor from the host.
	 **/
	

	vq = crdev->vq;
	sg_init_one(&syscall_type_sg, &syscall_type, sizeof(syscall_type));
	sgs[num_out++] = &syscall_type_sg;

	sg_init_one(&fd_sg, &host_fd, sizeof(host_fd));
	sgs[num_out + num_in++] = &fd_sg;

	/**
	 * Wait for the host to process our data.
	 **/
	

	spin_lock_irq(&crdev->lock);
	err = virtqueue_add_sgs(crdev->vq, sgs, num_out, num_in, 
	                        &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(crdev->vq);
	while (virtqueue_get_buf(crdev->vq, &len) == NULL)
		;
	spin_unlock_irq(&crdev->lock);


	/* If host failed to open() return -ENODEV. */
	
	debug("file descriptor:%d",host_fd);
	if (host_fd < 0) {
		return -ENODEV;
		goto fail;
	}
	crof->host_fd = host_fd;

fail:
	debug("Leaving");
	return ret;
}

static int crypto_chrdev_release(struct inode *inode, struct file *filp)
{
	int ret = 0;
	int err;
	unsigned int len;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	unsigned int syscall_type = VIRTIO_CRYPTO_SYSCALL_CLOSE;

	int host_fd = crof->host_fd;

	struct scatterlist syscall_type_sg, fd_sg, 
						*sgs[2];

	unsigned int num_out, num_in;

	debug("Entering");

	num_out = 0;
	num_in = 0;

	/**
	 * Send data to the host.
	 **/
	
	sg_init_one(&syscall_type_sg, &syscall_type, sizeof(syscall_type));
	sgs[num_out++] = &syscall_type_sg;

	sg_init_one(&fd_sg, &host_fd, sizeof(host_fd));
	sgs[num_out++] = &fd_sg;

	/**
	 * Wait for the host to process our data.
	 **/
	
	spin_lock_irq(&crdev->lock);
	err = virtqueue_add_sgs(crdev->vq, sgs, num_out, num_in, 
	                        &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(crdev->vq);
	while (virtqueue_get_buf(crdev->vq, &len) == NULL)
		;
	spin_unlock_irq(&crdev->lock);

	kfree(crof);
	debug("Leaving");
	return ret;

}

static long crypto_chrdev_ioctl(struct file *filp, unsigned int cmd, 
                                unsigned long arg)
{
	long ret = 0;
	int err;
	int i;
	struct crypto_open_file *crof = filp->private_data;
	struct crypto_device *crdev = crof->crdev;
	struct virtqueue *vq = crdev->vq;
	struct scatterlist syscall_type_sg, fd_sg, ioctl_cmd_sg, session_key_sg, session_op_sg, 
						host_return_val_sg, ses_id_sg, crypt_op_sg, src_sg, iv_sg, dst_sg, 
	                   *sgs[8];

	unsigned int num_out, num_in,
	             syscall_type = VIRTIO_CRYPTO_SYSCALL_IOCTL,
	             len;

	int host_fd = crof->host_fd;
	int ioctl_cmd;
	int DATA_SIZE=1;
	unsigned char session_key[KEY_SIZE];
	unsigned char *src;
	unsigned char *dst;
	
	unsigned char iv[BLOCK_SZ];
	struct session_op session_op;
	struct crypt_op crypt_op;
	int host_return_val;
	uint32_t ses_id;

	debug("Entering");
	dst = kzalloc(sizeof(*dst),GFP_KERNEL);
	num_out = 0;
	num_in = 0;

	sg_init_one(&syscall_type_sg, &syscall_type, sizeof(syscall_type));
	sgs[num_out++] = &syscall_type_sg;
	

	switch (cmd) {
	case CIOCGSESSION:
		debug("CIOCGSESSION");
		ioctl_cmd = 1;
		if(copy_from_user((void *)&session_op, (void *)arg, sizeof(struct session_op)))
			return -EACCES;
		if(copy_from_user((void *)session_key, (void *)session_op.key, KEY_SIZE))
			return -EACCES;
		
		sg_init_one(&fd_sg, &host_fd, sizeof(host_fd));
			sgs[num_out++] = &fd_sg;
		sg_init_one(&ioctl_cmd_sg, &ioctl_cmd, sizeof(ioctl_cmd));
			sgs[num_out++] = &ioctl_cmd_sg;
		sg_init_one(&session_key_sg, &session_key, sizeof(session_key));
			sgs[num_out++] = &session_key_sg;
		sg_init_one(&session_op_sg, &session_op, sizeof(session_op));
			sgs[num_out + num_in++] = &session_op_sg;
		sg_init_one(&host_return_val_sg, &host_return_val, sizeof(host_return_val));
			sgs[num_out + num_in++] = &host_return_val_sg;
	break;
	case CIOCFSESSION:
		debug("CIOCFSESSION");
		ioctl_cmd = 2;
		if(copy_from_user((void *)&ses_id, (void *)arg, sizeof(ses_id)))
			return -EACCES;
		debug("sesssion_id:%d",ses_id);
		sg_init_one(&fd_sg, &host_fd, sizeof(host_fd));
			sgs[num_out++] = &fd_sg;
		sg_init_one(&ioctl_cmd_sg, &ioctl_cmd, sizeof(ioctl_cmd));
			sgs[num_out++] = &ioctl_cmd_sg;
		sg_init_one(&ses_id_sg, &ses_id, sizeof(ses_id));
			sgs[num_out++] = &ses_id_sg;
		sg_init_one(&host_return_val_sg, &host_return_val, sizeof(host_return_val));
			sgs[num_out + num_in++] = &host_return_val_sg;
	break;
	case CIOCCRYPT:
		debug("CIOCCRYPT");
		ioctl_cmd = 3;
		if(copy_from_user((void *)&crypt_op, (void *)arg, sizeof(struct crypt_op)))
			return -EINVAL;
		DATA_SIZE = crypt_op.len;
		src = kzalloc(DATA_SIZE*sizeof(*src), GFP_KERNEL);
		dst = kzalloc(DATA_SIZE*sizeof(*dst), GFP_KERNEL);
		
		if(copy_from_user(src, (void *)crypt_op.src, DATA_SIZE))
			return -EINVAL;
		if(copy_from_user(iv, (void *)crypt_op.iv, BLOCK_SZ))
			return -EINVAL;
		
		
		
	
		sg_init_one(&fd_sg, &host_fd, sizeof(host_fd));
			sgs[num_out++] = &fd_sg;
		sg_init_one(&ioctl_cmd_sg, &ioctl_cmd, sizeof(ioctl_cmd));
			sgs[num_out++] = &ioctl_cmd_sg;
		sg_init_one(&crypt_op_sg, &crypt_op, sizeof(struct crypt_op));
			sgs[num_out++] = &crypt_op_sg;
		sg_init_one(&src_sg, src, sizeof(DATA_SIZE));
			sgs[num_out++] = &src_sg;
		sg_init_one(&iv_sg, iv, sizeof(BLOCK_SZ));
			sgs[num_out++] = &iv_sg;
		sg_init_one(&dst_sg, dst, sizeof(DATA_SIZE));
			sgs[num_out + num_in++] = &dst_sg;
		sg_init_one(&host_return_val_sg, &host_return_val, sizeof(host_return_val));
			sgs[num_out + num_in++] = &host_return_val_sg;
		break;
	default:
		debug("Unsupported ioctl command");
		break;		
	}

	spin_lock_irq(&crdev->lock);
	err = virtqueue_add_sgs(crdev->vq, sgs, num_out, num_in, 
	                        &syscall_type_sg, GFP_ATOMIC);
	virtqueue_kick(crdev->vq);
	while (virtqueue_get_buf(crdev->vq, &len) == NULL)
			;
	spin_unlock_irq(&crdev->lock);

	switch (cmd) {
		case CIOCGSESSION:
			debug("host_return_val:%d",host_return_val);
			debug("session id:%d",session_op.ses);
			if (host_return_val<0)
				return -EINVAL;
			if (copy_to_user((void *)arg, &session_op, sizeof(struct session_op)))
				return -EACCES;
		break;
	case CIOCFSESSION:
			debug("host_return_val:%d",host_return_val);
			if (host_return_val<0)
				return -EINVAL;
		break;
		case CIOCCRYPT:
			debug("host_return_val:%d",host_return_val);
			if (host_return_val<0)
				return -EINVAL;
			
			if (copy_to_user(crypt_op.dst, dst, DATA_SIZE))
				return -EACCES;
			if (copy_to_user((void *)arg, &crypt_op, sizeof(struct crypt_op)))
				return -EACCES;
			debug("test4");
		break;
	}
	
	debug("Leaving");
	debug("ret:%ld\n",ret);
	return 	ret;
}

static ssize_t crypto_chrdev_read(struct file *filp, char __user *usrbuf, 
                                  size_t cnt, loff_t *f_pos)
{
	debug("Entering");
	debug("Leaving");
	return -EINVAL;
}

static struct file_operations crypto_chrdev_fops = 
{
	.owner          = THIS_MODULE,
	.open           = crypto_chrdev_open,
	.release        = crypto_chrdev_release,
	.read           = crypto_chrdev_read,
	.unlocked_ioctl = crypto_chrdev_ioctl,
};

int crypto_chrdev_init(void)
{
	int ret;
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;
	
	debug("Initializing character device...");


	cdev_init(&crypto_chrdev_cdev, &crypto_chrdev_fops);
	crypto_chrdev_cdev.owner = THIS_MODULE;
	
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	ret = register_chrdev_region(dev_no, crypto_minor_cnt, "crypto_devs");
	if (ret < 0) {
		debug("failed to register region, ret = %d", ret);
		goto out;
	}
	ret = cdev_add(&crypto_chrdev_cdev, dev_no, crypto_minor_cnt);
	if (ret < 0) {
		debug("failed to add character device");
		goto out_with_chrdev_region;
	}

	debug("Completed successfully");
	return 0;

out_with_chrdev_region:
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
out:
	return ret;
}

void crypto_chrdev_destroy(void)
{
	dev_t dev_no;
	unsigned int crypto_minor_cnt = CRYPTO_NR_DEVICES;

	debug("entering");
	dev_no = MKDEV(CRYPTO_CHRDEV_MAJOR, 0);
	cdev_del(&crypto_chrdev_cdev);
	unregister_chrdev_region(dev_no, crypto_minor_cnt);
	debug("leaving");
}
