/*
 * Virtio Crypto Device
 *
 * Implementation of virtio-crypto qemu backend device.
 *
 * Dimitris Siakavaras <jimsiak@cslab.ece.ntua.gr>
 * Stefanos Gerangelos <sgerag@cslab.ece.ntua.gr> 
 *
 */

#include <qemu/iov.h>
#include "hw/virtio/virtio-serial.h"
#include "hw/virtio/virtio-crypto.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <crypto/cryptodev.h>


#define BLOCK_SIZE      16
#define KEY_SIZE        24

static uint32_t get_features(VirtIODevice *vdev, uint32_t features)
{
	DEBUG_IN();
	return features;
}

static void get_config(VirtIODevice *vdev, uint8_t *config_data)
{
	DEBUG_IN();
}

static void set_config(VirtIODevice *vdev, const uint8_t *config_data)
{
	DEBUG_IN();
}

static void set_status(VirtIODevice *vdev, uint8_t status)
{
	DEBUG_IN();
}

static void vser_reset(VirtIODevice *vdev)
{
	DEBUG_IN();
}

static void vq_handle_output(VirtIODevice *vdev, VirtQueue *vq)
{
	VirtQueueElement elem;
	unsigned int *syscall_type, *ioctl_cmd;
	int i, cfd, return_val;
	unsigned char session_key[KEY_SIZE];
	struct session_op session_op;
	struct crypt_op crypt_op;
	struct crypt_op cryp;
	unsigned char *src;
	unsigned char *dst;
	unsigned char iv[BLOCK_SIZE];
	uint32_t ses_id;
	int DATA_SIZE;

	DEBUG_IN();

	if (!virtqueue_pop(vq, &elem)) {
		DEBUG("No item to pop from VQ :(");
		return;
	} 

	DEBUG("I have got an item from VQ :)");

	syscall_type = elem.out_sg[0].iov_base;
	switch (*syscall_type) {
	case VIRTIO_CRYPTO_SYSCALL_TYPE_OPEN:
		DEBUG("VIRTIO_CRYPTO_SYSCALL_TYPE_OPEN");

		cfd = open("/dev/crypto", O_RDWR, 0);
		if (cfd < 0) {
			perror("open:(/dev/crypto)");
		}

		memcpy(elem.in_sg[0].iov_base, &cfd, sizeof(cfd));
		break;
	case VIRTIO_CRYPTO_SYSCALL_TYPE_CLOSE:
		DEBUG("VIRTIO_CRYPTO_SYSCALL_TYPE_CLOSE");
		
		memcpy(&cfd, elem.out_sg[1].iov_base, sizeof(cfd));
		
		if (close(cfd)) {
			perror("close(fd)");
		}
		break;
	case VIRTIO_CRYPTO_SYSCALL_TYPE_IOCTL:
		DEBUG("VIRTIO_CRYPTO_SYSCALL_TYPE_IOCTL");
		
		memcpy(&cfd, elem.out_sg[1].iov_base, sizeof(cfd));
		
		
		ioctl_cmd = elem.out_sg[2].iov_base;
		if (*ioctl_cmd == 1) {
			session_op.cipher = CRYPTO_AES_CBC;
			session_op.keylen = KEY_SIZE;
			session_op.key = elem.out_sg[3].iov_base;

			

			if (ioctl(cfd, CIOCGSESSION, &session_op)) {
				perror("ioctl(CIOCGSESSION)");
				return_val = -1;
			}
			else
				return_val = 0;
			
			memcpy(elem.in_sg[0].iov_base, &session_op, sizeof(struct session_op));
			memcpy(elem.in_sg[1].iov_base, &return_val, sizeof(return_val));
		}
		else if (*ioctl_cmd == 2) {
			memcpy(&ses_id, elem.out_sg[3].iov_base, sizeof(ses_id));
			
			if (ioctl(cfd, CIOCFSESSION, &ses_id)) {
				perror("ioctl(CIOCFSESSION)");
				return_val = -1;
			}
			else
				return_val = 0;
			memcpy(elem.in_sg[0].iov_base, &return_val, sizeof(return_val));
		}
		else {
				
			memcpy(&crypt_op, elem.out_sg[3].iov_base, sizeof(struct crypt_op));
			DATA_SIZE = crypt_op.len;
			src = (unsigned char *) malloc(DATA_SIZE*sizeof(*src));
			dst = (unsigned char *) malloc(DATA_SIZE*sizeof(*dst));
			memcpy(src, elem.out_sg[4].iov_base, DATA_SIZE*sizeof(*src));
			memcpy(iv, elem.out_sg[5].iov_base, sizeof(iv));
			
			
			
			cryp.ses = crypt_op.ses;
			cryp.len = DATA_SIZE;
			cryp.src = src;
			cryp.dst = dst;
			cryp.iv = iv;
			cryp.op = crypt_op.op;			 
			
			if (ioctl(cfd, CIOCCRYPT, &cryp)) {
				perror("ioctl(CIOCCRYPT)");
				return_val = -1;
			}
			else
				return_val = 0;
			
			memcpy(elem.in_sg[0].iov_base, dst, DATA_SIZE);
			memcpy(elem.in_sg[1].iov_base, &return_val, sizeof(return_val));
			
		}
		break;
	default:
		DEBUG("Unknown syscall_type");
	}
	virtqueue_push(vq, &elem, 0);
	virtio_notify(vdev, vq);
			
}

static void virtio_crypto_realize(DeviceState *dev, Error **errp)
{
    VirtIODevice *vdev = VIRTIO_DEVICE(dev);

	DEBUG_IN();

    virtio_init(vdev, "virtio-crypto", 13, 0);
	virtio_add_queue(vdev, 128, vq_handle_output);
}

static void virtio_crypto_unrealize(DeviceState *dev, Error **errp)
{
	DEBUG_IN();
}

static Property virtio_crypto_properties[] = {
    DEFINE_PROP_END_OF_LIST(),
};

static void virtio_crypto_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    VirtioDeviceClass *k = VIRTIO_DEVICE_CLASS(klass);

	DEBUG_IN();
    dc->props = virtio_crypto_properties;
    set_bit(DEVICE_CATEGORY_INPUT, dc->categories);

    k->realize = virtio_crypto_realize;
    k->unrealize = virtio_crypto_unrealize;
    k->get_features = get_features;
    k->get_config = get_config;
    k->set_config = set_config;
    k->set_status = set_status;
    k->reset = vser_reset;
}

static const TypeInfo virtio_crypto_info = {
    .name          = TYPE_VIRTIO_CRYPTO,
    .parent        = TYPE_VIRTIO_DEVICE,
    .instance_size = sizeof(VirtCrypto),
    .class_init    = virtio_crypto_class_init,
};

static void virtio_crypto_register_types(void)
{
    type_register_static(&virtio_crypto_info);
}

type_init(virtio_crypto_register_types)
