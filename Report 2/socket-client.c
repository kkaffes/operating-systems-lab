/*
 * socket-client.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <fcntl.h>
#include <crypto/cryptodev.h>

int sd, cfd;
void sig_handler(int signo) {
	if (signo == SIGINT) {
		if (shutdown(sd, SHUT_WR) < 0) {
			perror("shutdown");
			exit(1);
		}
		if (close(cfd) < 0) {
			perror("close(fd)");
			exit(1);
		}
		fprintf(stderr, "\nDone.\n");
		exit(0);
	}
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;
	
	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt;
}

ssize_t insist_read(int fd, void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = read(fd, buf, cnt);
                if (ret == 0)
                	return 0;
                if (ret < 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}


int main(int argc, char *argv[])
{
	int port, i;
	ssize_t n;
	char buf[DATA_SIZE];
	char *hostname;
	struct hostent *hp;
	struct sockaddr_in sa;
	struct session_op sess;
	struct crypt_op cryp;
	struct {
		unsigned char 	in[DATA_SIZE],
				encrypted[DATA_SIZE],
				decrypted[DATA_SIZE],
				iv[BLOCK_SIZE],
				key[KEY_SIZE];
	}data;
	char *filename;

	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));

	signal(SIGINT, sig_handler);
	
	filename = (argv[3] == NULL) ? "/dev/crypto" : argv[3];
	cfd = open(filename, O_RDWR);
	if (cfd < 0) {
		perror("open");
		return 1;
	}
	
	hostname = argv[1];
	port = atoi(argv[2]); /* Needs better error checking */

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");
	
	/* Look up remote hostname on DNS */
	if ( !(hp = gethostbyname(hostname))) {
		printf("DNS lookup failed for host %s\n", hostname);
		exit(1);
	}

	/* Connect to remote TCP port */
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	memcpy(&sa.sin_addr.s_addr, hp->h_addr, sizeof(struct in_addr));
	fprintf(stderr, "Connecting to remote host... "); fflush(stderr);
	if (connect(sd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		perror("connect");
		exit(1);
	}
	fprintf(stderr, "Connected.\n");

	memset(&data.key, 0, sizeof(data.key));
	memset(&data.iv, 0, sizeof(data.iv));		
	

	/*
	* Get crypto session for AES128
	*/
	sess.cipher = CRYPTO_AES_CBC;
	sess.keylen = KEY_SIZE;
	sess.key = data.key;

	if (ioctl(cfd, CIOCGSESSION, &sess)) {
		perror("ioctl(CIOCGSESSION)");
		return 1;
	}

	int max_fd;
	fd_set rd_set, temp_rd_set;
	max_fd = sd;
    FD_ZERO(&rd_set);
    FD_SET(sd, &temp_rd_set);
    FD_SET(0, &temp_rd_set);
	for (;;) {
		memcpy(&rd_set, &temp_rd_set, sizeof(rd_set));
    	if(select(max_fd+1, &rd_set, (fd_set *)NULL, (fd_set *)NULL, (struct timeval*)NULL)<0) {
    		perror("select");
    		break;
    	}
    	if(FD_ISSET(sd, &rd_set)) {
    		n = insist_read(sd, buf, DATA_SIZE);
    		if (n < 0) {
    			perror("read");
				exit(1);			
    		}
    		else if (n == 0)
    			break;

    		for (i = 0; i < n; i++)
    			data.in[i] = buf[i];

			cryp.ses = sess.ses;
			cryp.len = sizeof(data.in);
			cryp.iv = data.iv;
			cryp.src = data.in;
			cryp.dst = data.decrypted;
			cryp.op = COP_DECRYPT;
			if (ioctl(cfd, CIOCCRYPT, &cryp)) {
				perror("ioctl(CIOCCRYPT)");
				break;
			}

	   		int mes_bytes = data.decrypted[0]+10*data.decrypted[1]+100*data.decrypted[2];
			for (i=3; i<mes_bytes+3; i++)
				printf("%c",data.decrypted[i]);
    	}
    	if (FD_ISSET(0, &rd_set)) {
  			int bytes_read;
			  	size_t nbytes = 0;
  				char *my_string = NULL;
  				bytes_read = getline(&my_string, &nbytes, stdin);
  				if (bytes_read == -1) {
      				puts("getline");
      				exit(1);
    			}
    			if (bytes_read > 253) {
    				puts("Warning: maximum length of each message is 253 bytes");
    				continue;
    			}
	  			for (i = 0; i < bytes_read; i++)
	  				data.in[i+3] = my_string[i];

	  			char dig[3];
	  			i=0;
	  			memset(&dig,0,sizeof(dig));

	  			while(bytes_read) {
	  				dig[i++] = bytes_read % 10;
	  				bytes_read /= 10;
	  			}
	  			memcpy(data.in,dig,sizeof(dig));

	  			cryp.ses = sess.ses;
				cryp.len = DATA_SIZE;
				cryp.src = data.in;
				cryp.dst = data.encrypted;
				cryp.iv = data.iv;
				cryp.op = COP_ENCRYPT;
				if (ioctl(cfd, CIOCCRYPT, &cryp)) {
					perror("ioctl(CIOCCRYPT)");
					exit(1);
				};

				if (insist_write(sd, data.encrypted, DATA_SIZE) != DATA_SIZE) {
					perror("write");
					exit(1);
				}
				free(my_string);

				cryp.ses = sess.ses;
				cryp.len = sizeof(data.encrypted);
				cryp.iv = data.iv;
				cryp.src = data.encrypted;
				cryp.dst = data.decrypted;
				cryp.op = COP_DECRYPT;
				if (ioctl(cfd, CIOCCRYPT, &cryp)) {
					perror("ioctl(CIOCCRYPT)");
					return 1;
				}
		}
	}

	if (close(sd)<0){
		perror("close");
		exit(1);
	}
	if (close(cfd)<0){
		perror("close");
		exit(1);
	}
	
	return 0;
}
