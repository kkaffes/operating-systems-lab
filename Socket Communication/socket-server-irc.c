/*
 * socket-server.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <fcntl.h>
#include <crypto/cryptodev.h>

struct client {
	int newsd;
	int alive;
};
void error(const char *msg)
{
    perror(msg);
    exit(0);
}

/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;
	
	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	        	return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt;
}

ssize_t insist_read(int fd, void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = read(fd, buf, cnt);
                if (ret <= 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}

static int fill_urandom_buf(unsigned char *buf, size_t cnt)
{
        int crypto_fd;
        int ret = -1;

        crypto_fd = open("/dev/urandom", O_RDONLY);
        if (crypto_fd < 0)
                return crypto_fd;

        ret = insist_read(crypto_fd, buf, cnt);
        close(crypto_fd);

        return ret;
}

int main(int argc, char *argv[])
{
	char buf[100];
	ssize_t n;
	char addrstr[INET_ADDRSTRLEN];
	int sd, i, cons=0;
	struct client clnt[100];
	socklen_t len;
	struct sockaddr_in sa;
	
	struct session_op sess;
	struct crypt_op cryp;
	struct {
		unsigned char 	in[DATA_SIZE],
				encrypted[DATA_SIZE],
				decrypted[DATA_SIZE],
				iv[BLOCK_SIZE],
				key[KEY_SIZE];
	} data;

	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));
	memset(&data.in, 0, sizeof(data.in));
	memset(&data.key, 0, sizeof(data.key));
	memset(&data.key, 0, sizeof(data.key));
	memset(&data.iv, 0, sizeof(data.iv));

	int cfd;
	cfd = open("/dev/crypto", O_RDWR);
	if (cfd < 0) {
		perror("open(/dev/crypto)");
		return 1;
	}

	/* Make sure a broken connection doesn't kill us */
	signal(SIGPIPE, SIG_IGN);

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	int reuseaddr = 1;
	if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr)) <0) {
		perror("setsockopt");
		exit(1);
	}

	/* Bind to a well-known port */
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(TCP_PORT);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
		perror("bind");
		exit(1);
	}
	fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

	/* Listen for incoming connections */
	if (listen(sd, TCP_BACKLOG) < 0) {
		perror("listen");
		exit(1);
	}
for (;;) {
		/* Loop forever, accept()ing connections */
		fprintf(stderr, "Waiting for connections...\n");

		/* Accept an incoming connection */
		len = sizeof(struct sockaddr_in);
		if ((clnt[cons++].newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
			perror("accept");
			exit(1);
		}
		clnt[cons-1].alive = 1;

		if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
			perror("could not format IP address");
			exit(1);
		}
		fprintf(stderr, "Incoming connection from %s:%d\n",
			addrstr, ntohs(sa.sin_port));
		
		for (i = 0; i < BLOCK_SIZE; i++)
			printf("%x", data.iv[i]);
		printf("\n");
		for (i = 0; i < KEY_SIZE; i++)
			printf("%x", data.key[i]);
		printf("\n");

		/*
	 	* Get crypto session for AES128
	 	*/
		sess.cipher = CRYPTO_AES_CBC;
		sess.keylen = KEY_SIZE;
		sess.key = data.key;
		if (ioctl(cfd, CIOCGSESSION, &sess)) {
			perror("ioctl(CIOCGSESSION)");
			return 1;
		}
		/* We break out of the loop when the remote peer goes away */
		int max_fd;
		fd_set rd_set, temp_rd_set;
		max_fd = clnt[0].newsd;
   		FD_ZERO(&rd_set);
   		FD_SET(clnt[0].newsd, &temp_rd_set);
   		FD_SET(0, &temp_rd_set);
   		FD_SET(sd, &temp_rd_set);
		for (;;) {
			memcpy(&rd_set, &temp_rd_set, sizeof(rd_set));
   			if(select(max_fd+1, &rd_set, (fd_set *)NULL, (fd_set *)NULL, (struct timeval*)NULL)<0) {
   				perror("select");
    			break;
   			}

   			if(FD_ISSET(sd, &rd_set)) {
   				len = sizeof(struct sockaddr_in);
				if ((clnt[cons++].newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
					perror("accept");
					exit(1);
				}

				clnt[cons-1].alive = 1;

				if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
					perror("could not format IP address");
					exit(1);
				}
				fprintf(stderr, "Incoming connection from %s:%d\n",
					addrstr, ntohs(sa.sin_port));
				FD_SET(clnt[cons-1].newsd , &temp_rd_set);
				if (clnt[cons-1].newsd > max_fd)
					max_fd = clnt[cons-1].newsd;
   			}
   			for(i=0; i<cons; i++) {
   				if ( clnt[i].alive) {
   					int csd = clnt[i].newsd, j;
   					if(FD_ISSET(clnt[i].newsd, &rd_set)) {
	    				n = insist_read(clnt[i].newsd, buf, DATA_SIZE);
    					if (n < 0) {
		    				perror("read");
							exit(1);			
    					}
    					else if (n == 0) {
			    			puts("someone is dead");
			    			clnt[i].alive = 0;
		    				continue;
    					}
		    			for(j=0; j<cons; j++) {
			    			if (clnt[j].newsd != csd && clnt[j].alive)
    							if (insist_write(clnt[j].newsd, buf, DATA_SIZE) != DATA_SIZE) {
									perror("write");
									exit(1);
								}
						}
   					}
   				}	
   			}
		}
	}
	/* Make sure we don't leak open files */
	if (close(clnt[0].newsd) < 0) {
		perror("close");
		return 1;
	}
	if (close(clnt[0].newsd) < 0) {
		perror("close");
		return 1;
	}

	if (close(cfd) < 0) {
		perror("close(cfd)");
		return 1;
	}
	
	return 0;
}