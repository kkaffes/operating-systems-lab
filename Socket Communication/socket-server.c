/*
 * socket-server.c
 * Simple TCP/IP communication using sockets
 *
 * Vangelis Koukis <vkoukis@cslab.ece.ntua.gr>
 */

#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include "socket-common.h"

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <fcntl.h>
#include <crypto/cryptodev.h>

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

/* Insist until all of the data has been written */
ssize_t insist_write(int fd, const void *buf, size_t cnt)
{
	ssize_t ret;
	size_t orig_cnt = cnt;
	
	while (cnt > 0) {
	        ret = write(fd, buf, cnt);
	        if (ret < 0)
	                return ret;
	        buf += ret;
	        cnt -= ret;
	}

	return orig_cnt;
}

ssize_t insist_read(int fd, void *buf, size_t cnt)
{
        ssize_t ret;
        size_t orig_cnt = cnt;

        while (cnt > 0) {
                ret = read(fd, buf, cnt);
                if (ret < 0)
                        return ret;
                buf += ret;
                cnt -= ret;
        }

        return orig_cnt;
}



int main(int argc, char *argv[])
{
	char buf[100];
	ssize_t n;
	char addrstr[INET_ADDRSTRLEN];
	int sd, newsd, i, debug;
	socklen_t len;
	struct sockaddr_in sa;
	
	struct session_op sess;
	struct crypt_op cryp;
	struct {
		unsigned char 	in[DATA_SIZE],
				encrypted[DATA_SIZE],
				decrypted[DATA_SIZE],
				iv[BLOCK_SIZE],
				key[KEY_SIZE];
	} data;

	if (argc == 2 && strcmp(argv[1],"-d")==0)
		debug = 1;
	else if (argc == 1)
		debug = 0;
	else {
		fprintf(stderr, "Usage : %s (-d: debug)\n", argv[0]);
		exit(0);
	}

	memset(&sess, 0, sizeof(sess));
	memset(&cryp, 0, sizeof(cryp));
	memset(&data.in, 0, sizeof(data.in));
	memset(&data.key, 0, sizeof(data.key));
	memset(&data.key, 0, sizeof(data.key));
	memset(&data.iv, 0, sizeof(data.iv));

	int cfd;
	cfd = open("/dev/crypto", O_RDWR);
	if (cfd < 0) {
		perror("open(/dev/crypto)");
		return 1;
	}

	/* Make sure a broken connection doesn't kill us */
	signal(SIGPIPE, SIG_IGN);

	/* Create TCP/IP socket, used as main chat channel */
	if ((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	fprintf(stderr, "Created TCP socket\n");

	int reuseaddr = 1;
	if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr)) <0) {
		perror("setsockopt");
		exit(1);
	}

	/* Bind to a well-known port */
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(TCP_PORT);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
		perror("bind");
		exit(1);
	}
	fprintf(stderr, "Bound TCP socket to port %d\n", TCP_PORT);

	/* Listen for incoming connections */
	if (listen(sd, TCP_BACKLOG) < 0) {
		perror("listen");
		exit(1);
	}
for (;;) {
		/* Loop forever, accept()ing connections */
		fprintf(stderr, "Waiting for connections...\n");

		/* Accept an incoming connection */
		len = sizeof(struct sockaddr_in);
		if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
			perror("accept");
			exit(1);
		}
		if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
			perror("could not format IP address");
			exit(1);
		}
		fprintf(stderr, "Incoming connection from %s:%d\n",
			addrstr, ntohs(sa.sin_port));
		
		for (i = 0; i < BLOCK_SIZE; i++)
			printf("%x", data.iv[i]);
		printf("\n");
		for (i = 0; i < KEY_SIZE; i++)
			printf("%x", data.key[i]);
		printf("\n");

		/*
	 	* Get crypto session for AES128
	 	*/
		sess.cipher = CRYPTO_AES_CBC;
		sess.keylen = KEY_SIZE;
		sess.key = data.key;
		if (ioctl(cfd, CIOCGSESSION, &sess)) {
			perror("ioctl(CIOCGSESSION)");
			return 1;
		}

		/* We break out of the loop when the remote peer goes away */
		int max_fd;
		fd_set rd_set, temp_rd_set;
		max_fd = newsd;
   		FD_ZERO(&rd_set);
   		FD_SET(newsd, &temp_rd_set);
   		FD_SET(0, &temp_rd_set);
   		FD_SET(sd, &temp_rd_set);
		for (;;) {
			memcpy(&rd_set, &temp_rd_set, sizeof(rd_set));
   			if(select(max_fd+1, &rd_set, (fd_set *)NULL, (fd_set *)NULL, (struct timeval*)NULL)<0) {
   				perror("select");
    			break;
   			}
   			if(FD_ISSET(sd, &rd_set)) {
   				len = sizeof(struct sockaddr_in);
				if ((newsd = accept(sd, (struct sockaddr *)&sa, &len)) < 0) {
					perror("accept");
					exit(1);
				}
				if (!inet_ntop(AF_INET, &sa.sin_addr, addrstr, sizeof(addrstr))) {
					perror("could not format IP address");
					exit(1);
				}
			fprintf(stderr, "Incoming connection from %s:%d\n",
			addrstr, ntohs(sa.sin_port));
   			}
   			if(FD_ISSET(newsd, &rd_set)) {
    			n = insist_read(newsd, buf, DATA_SIZE);
    		if (n < 0) {
    			perror("read");
				exit(1);			
    		}
    		else if (n == 0)
    			break;

    		for (i = 0; i < n; i++)
    			data.in[i] = buf[i];
    
    		if (debug) {
    			printf("\nData received:\n");
				for (i = 0; i < n; i++) {
					printf("%x", data.in[i]);
				}
				printf("\n");
			}

			cryp.ses = sess.ses;
			cryp.len = sizeof(data.in);
			cryp.iv = data.iv;
			cryp.src = data.in;
			cryp.dst = data.decrypted;
			cryp.op = COP_DECRYPT;
			if (ioctl(cfd, CIOCCRYPT, &cryp)) {
				perror("ioctl(CIOCCRYPT)");
				return 1;
			}

			if (debug) {
				printf("\nDecrypted data:\n");
				for (i = 0; i < n; i++) {
					printf("%x", data.decrypted[i]);
				}
				printf("\n");
			}	

	   		int mes_bytes = data.decrypted[0]+10*data.decrypted[1]+100*data.decrypted[2];
			for (i=3; i<mes_bytes+3; i++)
				printf("%c",data.decrypted[i]);
   			}
   			if (FD_ISSET(0, &rd_set)) {
   				int bytes_read;
			  	size_t nbytes = 0;
  				char *my_string = NULL;
  				bytes_read = getline(&my_string, &nbytes, stdin);
  				if (bytes_read == -1) {
      				puts("getline");
      				exit(1);
    			}

	  			for (i = 0; i < bytes_read; i++)
	  				data.in[i+3] = my_string[i];

	  			char dig[3];
	  			i=0;
	  			memset(&dig,0,sizeof(dig));

	  			while(bytes_read) {
	  				dig[i++] = bytes_read % 10;
	  				bytes_read /= 10;
	  			}
	  			memcpy(data.in,dig,sizeof(dig));

	  			if (debug) {
	  				printf("\nOriginal data\n");
	  				for (i = 0; i < DATA_SIZE; i++) {
						printf("%x", data.in[i]);
					}
					printf("\n");
				}

	  			cryp.ses = sess.ses;
				cryp.len = DATA_SIZE;
				cryp.src = data.in;
				cryp.dst = data.encrypted;
				cryp.iv = data.iv;
				cryp.op = COP_ENCRYPT;
				if (ioctl(cfd, CIOCCRYPT, &cryp)) {
					perror("ioctl(CIOCCRYPT)");
					exit(1);
				}
				if (debug) {
					printf("\nEncrypted data:\n");
					for (i = 0; i < DATA_SIZE; i++) {
						printf("%x", data.encrypted[i]);
					}
					printf("\n");
				}

				if (insist_write(newsd, data.encrypted, DATA_SIZE) != DATA_SIZE) {
					perror("write");
					exit(1);
				}
				free(my_string);

				cryp.ses = sess.ses;
				cryp.len = sizeof(data.encrypted);
				cryp.iv = data.iv;
				cryp.src = data.encrypted;
				cryp.dst = data.decrypted;
				cryp.op = COP_DECRYPT;
				if (ioctl(cfd, CIOCCRYPT, &cryp)) {
					perror("ioctl(CIOCCRYPT)");
					return 1;
				}
			}
		}
	}
	/* Make sure we don't leak open files */
	if (close(newsd) < 0)
		perror("close");

	if (close(cfd) < 0) {
		perror("close(cfd)");
		return 1;
	}
	
	return 0;
}